//
//  IntersectionSpec.swift
//  Intersection
//
//  Created by Anton Tikhonov on 4/29/17.
//  Copyright © 2017 Anton Tikhonov. All rights reserved.
//

import Foundation
import Quick
import Nimble
@testable import Intersection

class IntersectionSpec : QuickSpec {
    override func spec() {
        describe("make sure Intersection") {

            class AllRedProgram : IntersectionProgram {
                var name = "AllRed"
                var commands: [Command] = [
                    .SetAll(color: .Red, clocks: 1)
                ]
                var clockPeriod = 1.0
            }

            var x: Intersection!

            beforeEach {
                x = Intersection()
            }

            func expectAllLights(in color: TrafficLightsColor, on x: Intersection) {
                x.allTrafficLights().forEach {
                    expect($0).to(equal(color))
                }
            }

            func expectNorthSouthLights(in color: TrafficLightsColor, on x: Intersection) {
                x.northSouthTrafficLights().forEach {
                    expect($0).to(equal(color))
                }
            }

            func expectEastWestLights(in color: TrafficLightsColor, on x: Intersection) {
                x.eastWestTrafficLights().forEach {
                    expect($0).to(equal(color))
                }
            }

            it("has proper state after creation") {
                expect(x).toNot(beNil())
                expect(x.clock).to(equal(0))
                expect(x.nextCommandClock).to(equal(0))
                expect(x.nextProgramStep).to(equal(0))
                expectAllLights(in: .Off, on: x)
            }

            it("ignores clocks when no program is selected") {
                x.clock(3)

                expect(x.clock).to(equal(0))
                expectAllLights(in: .Off, on: x)
            }

            it("runs clock when a program is selected") {
                x.start(program: AllRedProgram())
                x.clock(3)

                expect(x.clock).to(equal(3))
                expectAllLights(in: .Red, on: x)
            }

            it("resets state when new program is selected") {
                x.start(program: BlinkingRedProgram())
                x.clock(1)

                x.start(program: DefaultIntersectionProgram())

                expect(x.clock).to(equal(0))
                expect(x.nextCommandClock).to(equal(0))
                expect(x.nextProgramStep).to(equal(0))
            }

            it("resets state when turned off") {
                x.start(program: AllRedProgram())
                x.clock(3)

                expectAllLights(in: .Red, on: x)

                x.off()

                expect(x.clock).to(equal(0))
                expect(x.nextCommandClock).to(equal(0))
                expect(x.nextProgramStep).to(equal(0))
                expectAllLights(in: .Off, on: x)
            }

            it("can run simple program") {
                class CheckLightsProgram : IntersectionProgram {
                    var name = "CheckLights"
                    var commands: [Command] = [
                        .SetAll(color: .Red, clocks: 1),
                        .SetAll(color: .Yellow, clocks: 2),
                        .SetAll(color: .Green, clocks: 3),
                    ]
                    var clockPeriod = 1.0
                }
                x.start(program: CheckLightsProgram())
                x.clock()
                expectAllLights(in: .Red, on: x)
                x.clock()
                expectAllLights(in: .Yellow, on: x)
                x.clock()
                expectAllLights(in: .Yellow, on: x)
                x.clock()
                expectAllLights(in: .Green, on: x)
                x.clock(3)
                expectAllLights(in: .Red, on: x)
                x.clock()
                expectAllLights(in: .Yellow, on: x)
            }

            it("can run blinking red program") {
                x.start(program: BlinkingRedProgram())
                x.clock()
                expectAllLights(in: .Red, on: x)
                x.clock(7)
                expectAllLights(in: .Off, on: x)
                x.clock(6)
                expectAllLights(in: .Red, on: x)
            }

            it("can run default program") {
                x.start(program: DefaultIntersectionProgram())
                x.clock()

                // That's our initial state.
                expectNorthSouthLights(in: .Red, on: x)
                expectEastWestLights(in: .Green, on: x)

                // Spin it around a few times...
                for _ in 1...10 {
                    x.clock(25)
                    expectNorthSouthLights(in: .Red, on: x)
                    expectEastWestLights(in: .Yellow, on: x)
                    x.clock(5)
                    expectNorthSouthLights(in: .Green, on: x)
                    expectEastWestLights(in: .Red, on: x)
                    x.clock(25)
                    expectNorthSouthLights(in: .Yellow, on: x)
                    expectEastWestLights(in: .Red, on: x)
                    x.clock(5)
                    expectNorthSouthLights(in: .Red, on: x)
                    expectEastWestLights(in: .Green, on: x)
                }
            }
        }
    }
}
