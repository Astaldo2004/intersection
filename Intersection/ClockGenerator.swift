//
//  ClockGenerator.swift
//  Intersection
//
//  Created by Anton Tikhonov on 4/30/17.
//  Copyright © 2017 Anton Tikhonov. All rights reserved.
//

import Foundation
import RxSwift

// Generates clocks that drive Intersection.
// Clocks are controlled by two things: period and divider.
class ClockGenerator {

    // Observables to wire up to UI.
    var running = Variable<Bool>(false)
    var divider = Variable<Int>(1)

    private var bag = DisposeBag()

    private var timer: Timer? = nil
    private var period = 1.0

    // We need to store timer's closure because we may need to restart the timer along the way
    // if divider gets changed.
    private typealias TimerClosure = (Void) -> Void
    private var timerClosure: TimerClosure? = nil

    init() {
        // Divider is changed by tapping a button on screen.
        // We can't change timer's period when it's already running. We have to re-create the timer.
        divider.asObservable()
            .subscribe({ [weak self] divider in
                guard let sself = self else { return }
                sself.restartTimer()
            })
            .addDisposableTo(bag)
    }

    deinit {
        stop()
    }

    // Starts the timer that will run the closure every (period / divider) seconds.
    func start(period: Double, closure: @escaping (Void) -> Void) {
        self.period = period
        timerClosure = closure
        running.value = true

        restartTimer()
    }

    // Stops the timer, releases everything.
    func stop() {
        timer?.invalidate()
        timer = nil
        timerClosure = nil
        running.value = false
    }

    private func restartTimer() {
        // Dispose the old timer if we have one.
        if let timer = timer {
            timer.invalidate()
        }

        // iOS 10 syntax for timer.
        // Our deployment target is set to iOS10, so it's okay.
        timer = Timer.scheduledTimer(withTimeInterval: period / Double(self.divider.value),
                                     repeats: true) { [weak self] _ in
                                        guard
                                            let sself = self,
                                            let closure = sself.timerClosure else { return }

                                        closure()
                                     }
    }
}
