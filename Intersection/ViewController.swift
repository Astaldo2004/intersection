//
//  ViewController.swift
//  Intersection
//
//  Created by Anton Tikhonov on 4/29/17.
//  Copyright © 2017 Anton Tikhonov. All rights reserved.
//

import UIKit
import RxSwift

class ViewController: UIViewController {

    let bag = DisposeBag()

    let intersection = Intersection()
    let clock = ClockGenerator()

    let redImage = UIImage(named: "red")
    let yellowImage = UIImage(named: "amber")
    let greenImage = UIImage(named: "green")
    let offImage = UIImage(named: "off")

    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var blinkingRedButton: UIButton!
    @IBOutlet weak var offButton: UIButton!
    @IBOutlet weak var speedButton: UIButton!

    @IBOutlet weak var northImageView: UIImageView!
    @IBOutlet weak var southImageView: UIImageView!
    @IBOutlet weak var eastImageView: UIImageView!
    @IBOutlet weak var westImageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()

        wireupUI()
        wireupIntersection()

        // Intersector starts with blinking red mode.
        startBlinkingRed()
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDeviceOrientationIsLandscape(UIDevice.current.orientation) {
            // When in landscape, rotate traffic lights to landscape to help with layout.
            [northImageView, southImageView, eastImageView, westImageView].forEach() { view in
                view?.transform = CGAffineTransform(rotationAngle: .pi/2)
            }
        } else {
            [northImageView, southImageView, eastImageView, westImageView].forEach() { view in
                view?.transform = .identity
            }
        }
    }

    func startBlinkingRed() {
        start(program: BlinkingRedProgram())
    }

    func startDefault() {
        start(program: DefaultIntersectionProgram())
    }

    func start(program: IntersectionProgram) {
        intersection.start(program: program)

        clock.start(period: program.clockPeriod) { [weak self] in
            guard let sself = self else { return }

            sself.intersection.clock()
        }
    }

    func stop() {
        clock.stop()
        clock.divider.value = 1
        intersection.off()
    }

    func wireupUI() {
        // Observe clock running state to enable/disable Stop button.
        clock.running.asObservable()
            .subscribe(onNext: { [weak self] running in
                guard let sself = self else { return }
                sself.offButton.isEnabled = running
            })
            .addDisposableTo(bag)

        // Observe clock divider to set animation speed button title.
        clock.divider.asObservable()
            .subscribe(onNext: { [weak self] divider in
                guard let sself = self else { return }
                sself.speedButton.setTitle("\(divider)x", for: .normal)
            })
            .addDisposableTo(bag)
    }
    
    func wireupIntersection() {
        // Start observing intersection traffic light so we can update UI accordingly.
        intersection.north.asObservable()
            .subscribe(onNext: { [weak self] color in
                guard let sself = self else { return }
                sself.set(image: sself.image(for: color), to: sself.northImageView)
            })
            .addDisposableTo(bag)

        intersection.south.asObservable()
            .subscribe(onNext: { [weak self] color in
                guard let sself = self else { return }
                sself.set(image: sself.image(for: color), to: sself.southImageView)
            })
            .addDisposableTo(bag)

        intersection.east.asObservable()
            .subscribe(onNext: { [weak self] color in
                guard let sself = self else { return }
                sself.set(image: sself.image(for: color), to: sself.eastImageView)
            })
            .addDisposableTo(bag)

        intersection.west.asObservable()
            .subscribe(onNext: { [weak self] color in
                guard let sself = self else { return }
                sself.set(image: sself.image(for: color), to: sself.westImageView)
            })
            .addDisposableTo(bag)

        intersection.programName.asObservable()
            .subscribe(onNext: { [weak self] (name: String?) in
                guard let sself = self else { return }

                let isBlinkingRedRunning = (name == "BlinkingRed")
                let isDefaultRunning = (name == "Default")

                sself.blinkingRedButton.isEnabled = !isBlinkingRedRunning
                sself.startButton.isEnabled = !isDefaultRunning
            })
            .addDisposableTo(bag)
    }

    func set(image: UIImage?, to imageView: UIImageView) {
        // Let's smooth out changing colors a little bit.
        // Regular UIView.animate(...) doesn't work with UIImage.image, but UIView.transition(...) does the job.
        UIView.transition(with: imageView,
                          duration: 0.1,
                          options: .transitionCrossDissolve,
                          animations: { imageView.image = image },
                          completion: nil)
    }

    func image(for color: TrafficLightsColor) -> UIImage? {
        switch color {
        case .Red:
            return redImage
        case .Yellow:
            return yellowImage
        case .Green:
            return greenImage
        case .Off:
            return offImage
        }
    }

    @IBAction func startTapped(_ sender: UIButton) {
        startDefault()
    }

    @IBAction func blinkingRedTapped(_ sender: UIButton) {
        startBlinkingRed()
    }

    @IBAction func offTapped(_ sender: UIButton) {
        stop()
    }

    @IBAction func speedTapped(_ sender: UIButton) {
        var newDiv = clock.divider.value + 1
        if newDiv > 5 {
            newDiv = 1
        }
        clock.divider.value = newDiv
    }
}

