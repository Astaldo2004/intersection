//
//  IntersectionProgram.swift
//  Intersection
//
//  Created by Anton Tikhonov on 4/29/17.
//  Copyright © 2017 Anton Tikhonov. All rights reserved.
//

import Foundation

// Intersection program needs to have a name, clock speed and commands to execute.
protocol IntersectionProgram {
    var name: String { get }
    var commands: [Command] { get }
    var clockPeriod: Double { get }
}

// Default program for intersector.
// Alternates lights every 30 seconds with yellow for 5 seconds between red and green.
class DefaultIntersectionProgram : IntersectionProgram {
    var name = "Default"
    var commands: [Command] = [
        .Set(northSouthColor: .Red, eastWestColor: .Green, clocks: 25),
        .Set(northSouthColor: .Red, eastWestColor: .Yellow, clocks: 5),
        .Set(northSouthColor: .Green, eastWestColor: .Red, clocks: 25),
        .Set(northSouthColor: .Yellow, eastWestColor: .Red, clocks: 5)
    ]
    var clockPeriod = 1.0 // second
}

// Implements "proceed with caution" blinking red mode to intersector.
class BlinkingRedProgram : IntersectionProgram {
    var name = "BlinkingRed"
    var commands: [Command] = [
        .SetAll(color: .Red, clocks: 7),
        .SetAll(color: .Off, clocks: 6)
    ]
    var clockPeriod = 0.1
}
