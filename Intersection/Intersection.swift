//
//  Intersection.swift
//  Intersection
//
//  Created by Anton Tikhonov on 4/29/17.
//  Copyright © 2017 Anton Tikhonov. All rights reserved.
//

import Foundation
import RxSwift

enum TrafficLightsColor {
    case Off
    case Green
    case Yellow
    case Red
}

// Commands that control traffic lights on intersection.
// Each command turns traffic lights to a specific state (like "all lights red")
// Each command lasts a certain number of clocks.
// The actual duration of each clock is determined by clock generator that drives intersection.
enum Command {
    case SetAll(color: TrafficLightsColor, clocks: Int)
    case Set(northSouthColor: TrafficLightsColor, eastWestColor: TrafficLightsColor, clocks: Int)
}

// Represents an intersection with 4 traffic lights.
class Intersection {

    // 4 traffic lights. Observable.
    var north = Variable<TrafficLightsColor>(.Off)
    var south = Variable<TrafficLightsColor>(.Off)
    var east = Variable<TrafficLightsColor>(.Off)
    var west = Variable<TrafficLightsColor>(.Off)

    private(set) var clock = 0
    private(set) var nextProgramStep = 0
    private(set) var nextCommandClock = 0

    // Current program to be "executed" by intersection.
    var program: IntersectionProgram? {
        didSet {
            // Reset intersection state when new program is assigned.
            clock = 0
            nextCommandClock = 0
            nextProgramStep = 0
            programName.value = program?.name
            if program == nil {
                [north, south, east, west].forEach() { color in
                    color.value = .Off
                }
            }
        }
    }
    // Observable program name allows UI to update itself.
    var programName = Variable<String?>(nil)

    // Assignes a new program to intersection.
    // For the program to actually run, clock() method needs to be called by clock generator.
    func start(program: IntersectionProgram) {
        self.program = program
    }

    func off() {
        program = nil
    }

    // Helpers to get colors of traffic lights.
    func allTrafficLights() -> [TrafficLightsColor] {
        return [north.value, south.value, east.value, west.value]
    }

    func northSouthTrafficLights() -> [TrafficLightsColor] {
        return [north.value, south.value]
    }

    func eastWestTrafficLights() -> [TrafficLightsColor] {
        return [east.value, west.value]
    }

    // It's like the "main" function of the intersector as it executes intersector program and drives intersector.
    // This method needs to be called multiple times sequentially until the intersector needs to stop.
    // Normally it's done ClockGenerator class which allows to control how often clock() is called.
    func clock(_ clocks: Int = 1) {
        guard let program = program else { return }

        for _ in (1...clocks) {
            let startNewCommand = (clock == nextCommandClock)
            if startNewCommand {
                let nextCommand = program.commands[nextProgramStep]
                nextProgramStep += 1

                run(command: nextCommand)

                // figure out if it's end of program and we need to loop back to beginning.
                let loop = (nextProgramStep == program.commands.count)
                if loop {
                    nextProgramStep = 0
                }
            }
            clock += 1
        }
    }

    // Helper that executes a single command.
    private func run(command: Command) {
        switch command {
        case .SetAll(let color, let clocks):
            [north, south, east, west].forEach() { $0.value = color }
            nextCommandClock = nextCommandClock + clocks
        case .Set(let northSouthColor, let eastWestColor, let clocks):
            north.value = northSouthColor
            south.value = northSouthColor
            east.value = eastWestColor
            west.value = eastWestColor
            nextCommandClock = nextCommandClock + clocks
        }
    }
}
